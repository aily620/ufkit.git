//
//  UFRowImageStyle.h
//  UFKit
//
//  Created by ChenZhangli QQ893419255 on 2019/9/26.
//  Copyright © 2019 UFKit. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class UFImageStyleMaker;
@interface UFImageStyle : NSObject

@property (nonatomic, strong, nullable) UIImage *placeholderImage;
@property (nonatomic, assign) CGSize imageSize;
@property (nonatomic, assign) CGFloat cornerRadius;
@property (nonatomic, assign) UIViewContentMode contentMode;

+ (UFImageStyle *)makeImageStyle:(NS_NOESCAPE void(^)(UFImageStyleMaker *make))block;

@end


@interface UFImageStyleMaker : NSObject

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithImageStyle:(UFImageStyle *)imageStyle NS_DESIGNATED_INITIALIZER;

@property (nonatomic, strong, readonly) UFImageStyle *imageStyle;

@property (nonatomic, copy, readonly) UFImageStyleMaker *(^placeholderImage)(UIImage *placeholderImage);
@property (nonatomic, copy, readonly) UFImageStyleMaker *(^imageSize)(CGSize imageSize);
@property (nonatomic, copy, readonly) UFImageStyleMaker *(^cornerRadius)(CGFloat cornerRadius);
@property (nonatomic, copy, readonly) UFImageStyleMaker *(^contentMode)(UIViewContentMode contentMode);

@end

NS_ASSUME_NONNULL_END
