//
//  UFCollectionViewRow.h
//  Created by ChenZhangli QQ893419255 on 2021/1/26.
//  Copyright © 2021 UFKit. All rights reserved.
//
//          _ _     _ _   _ _ _ _ _   _ _    _ _     _ _   _ _ _ _ _ _ __
//         /  /    /  /  /  _ _ _ /  /  /   ╱  ╱    /__/  /_ _ _   _ _ _/
//        /  /    /  /  /  /        /  /  ╱  ╱     _ _         /  /
//       /  /    /  /  /  /_ _ _   /  / ╱  ╱      /  /        /  /
//      /  /    /  /  /  _ _ _ /  /  / \  \      /  /        /  /
//     /  /_ __/  /  /  /        /  /   \  \    /  /        /  /
//     \ _ _ _ _ /  /__/        /__/     \__\  /__/        /__/

#import <UFKit/UFKit.h>

NS_ASSUME_NONNULL_BEGIN

@class UFCollectionViewRow;
typedef void(^UFRowItemDidSelected)(UFCollectionViewRow *row, id item, NSInteger index, UIImageView *imageView);
typedef void(^UFRowItemDidDelete)(UFCollectionViewRow *row, id item, NSInteger index);
typedef BOOL(^UFRowItemDidDeleteHiden)(UFCollectionViewRow *row, id item, NSInteger index);

@class UFCollectionViewRowMaker;
@interface UFCollectionViewRow : UFRow

/// 图集（支持NSString、UIImage）
@property (nonatomic, strong) NSMutableArray *itemArray;
/// 列数
@property (nonatomic, assign) NSInteger cloumnCount;
/// 点击每一项图片回调
@property (nonatomic, copy) UFRowItemDidSelected rowItemDidSelected;
/// 点击删除回调
@property (nonatomic, copy) UFRowItemDidDelete rowItemDidDelete;
/// 是否显示删除按钮
@property (nonatomic, copy) UFRowItemDidDeleteHiden rowItemDidDeleteHiden;

+ (UFRow *)makeRow:(NS_NOESCAPE void(^)(UFRowMaker *make))block NS_UNAVAILABLE;
+ (UFCollectionViewRow *)makeCollectionViewRow:(NS_NOESCAPE void(^)(UFCollectionViewRowMaker *make))block;

@end


@interface UFCollectionViewRowMaker : UFRowMaker

- (instancetype)init NS_UNAVAILABLE;
- (instancetype)initWithRow:(UFRow *)row NS_UNAVAILABLE;
- (instancetype)initWithCollectionViewRow:(UFCollectionViewRow *)row NS_DESIGNATED_INITIALIZER;

@property (nonatomic, strong, readonly) UFCollectionViewRow *collectionViewRow;

@property (nonatomic, copy, readonly) UFCollectionViewRowMaker *(^itemArray)(NSMutableArray * _Nullable itemArray);
@property (nonatomic, copy, readonly) UFCollectionViewRowMaker *(^cloumnCount)(NSInteger cloumnCount);

@property (nonatomic, copy, readonly) UFCollectionViewRowMaker *(^rowItemDidSelected)(UFRowItemDidSelected rowItemDidSelected);
@property (nonatomic, copy, readonly) UFCollectionViewRowMaker *(^rowItemDidDelete)(UFRowItemDidDelete rowItemDidDelete);
@property (nonatomic, copy, readonly) UFCollectionViewRowMaker *(^rowItemDidDeleteHiden)(UFRowItemDidDeleteHiden rowItemDidDeleteHiden);

@end

NS_ASSUME_NONNULL_END
