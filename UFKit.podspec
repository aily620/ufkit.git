
Pod::Spec.new do |spec|


  spec.name         = "UFKitPlus"
  spec.version      = "5.0.0"
  spec.summary      = "表单UI库"

  spec.description  = "一个表单UI库"

  spec.homepage     = "https://gitee.com/aily620/ufkit.git"
  spec.license      = "MIT"
  spec.author       = { "chenzhangli" => "chenzhangli@zhenghe.cn" }
  
  spec.platform = :ios, '9.0'
  spec.ios.deployment_target = "9.0"
  spec.pod_target_xcconfig = { 'VALID_ARCHS' => 'x86_64 armv7 arm64' }

  spec.source       = { :git => "https://gitee.com/aily620/ufkit.git", :tag => "#{spec.version}" }


  spec.source_files  = "UFKit.framework/Headers", "*.{h,m}"
  spec.vendored_frameworks = 'UFKit.framework'
  spec.resource = 'UFKit.bundle'

  spec.dependency 'Masonry', '~> 1.1.0'
  spec.dependency 'SDWebImage', '~> 5.0'
  spec.dependency 'IQKeyboardManager', '~> 6.3.0'
  
end
